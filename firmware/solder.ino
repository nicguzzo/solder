#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <PID_v1.h>
#define DEBUG_SERIAL
//#define USE_ATUNE
#if USE_ATUNE
#include "PID_AutoTune_v0.h"
#endif
#include <Encoder.h>
#include <Thermistor.h>

//#include <avr/pgmspace.h>

#define pwm1_pin 9
#define pwm1_max 255
#define pwm2_pin 10
#define pwm2_max 255
#define pwm3_pin 11
#define pwm3_max 255
#define RST 8   
#define temp1_pin A0
#define temp2_pin A1
#define temp3_pin A2
#define but1 4
#define but2 5
#define but3 6
#define but4 7
#define VREF_MV 4930
#define R1 1.0                                    // non-inverting amplifier R1 in kΩ
#define R2 47.0                                  // non-inverting amplifier R2 in kΩ
#define GAIN (1 + R2/R1)                          // non-inverting amplifier gain
#define MV_PER_ANALOG_DIVISION (VREF_MV / 1023.0)
#define CELCIUS_PER_MV (1.0 / 0.041)              // based on 41µV per ˚C linear approximation for Type K > 0˚C
const float typekReadingScaler = CELCIUS_PER_MV / GAIN * MV_PER_ANALOG_DIVISION;

Encoder knob(2, 3);
long knob_pos  = 0;
double temp1=0;
double temp1_setpoint=270;
double temp2=0;
double temp2_setpoint=200;
double old_temp1=0;
double old_temp2=0;
char selector=0;
char temp1_onoff=0;
char temp2_onoff=0;
double temp1_pwm=0;
double temp2_pwm=0;
double kp1=3,ki1=0.2,kd1=1.2;
double kp2=3,ki2=0.2,kd2=1.2;
int fan_pwm=0;

double temp_amb=0;
char but_state[]={0,0,0,0};
/*
const short temptable[] = { 
    611 , 100 ,
    641 ,  95 ,
    681 ,  90 ,
    711 ,  85 ,
    751 ,  79 ,
    791 ,  72 ,
    811 ,  69 ,
    831 ,  65 ,
    871 ,  57 ,
    881 ,  55 ,
    901 ,  51 ,
    921 ,  45 ,
    941 ,  39 ,
    971 ,  28 ,
    981 ,  23 ,
    991 ,  17 ,
   1001 ,   9 ,
   1021 , -27 
};*/

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels
// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#include <Thermistor.h>
#include <NTC_Thermistor.h>
#include <AverageThermistor.h>
#define SENSOR_PIN             A2
#define REFERENCE_RESISTANCE   4700
#define NOMINAL_RESISTANCE     100000
#define NOMINAL_TEMPERATURE    25
#define B_VALUE                3950
#define READINGS_NUMBER 4
#define DELAY_TIME 10
Thermistor* thermistor = NULL;

PID temp1PID(&temp1, &temp1_pwm, &temp1_setpoint,kp1,ki1,kd1, DIRECT);
PID temp2PID(&temp2, &temp2_pwm, &temp2_setpoint,kp2,ki2,kd2, DIRECT);
#if USE_ATUNE
PID_ATune aTune(&temp1, &temp1_pwm);
bool tuning=false,tuning_ended=false;
#endif

void setup()   {
  pinMode(pwm1_pin, OUTPUT);
  pinMode(pwm2_pin, OUTPUT);
  pinMode(pwm3_pin, OUTPUT);
  analogWrite(pwm1_pin, 0);
  analogWrite(pwm2_pin, 0);
  analogWrite(pwm3_pin, 0);
  pinMode(but1,INPUT_PULLUP);
  pinMode(but2,INPUT_PULLUP);
  pinMode(but3,INPUT_PULLUP);
  pinMode(but4,INPUT_PULLUP);
  pinMode(13, OUTPUT);
  
  Wire.begin();
  temp1PID.SetOutputLimits(0,pwm1_max);
  temp2PID.SetOutputLimits(0,pwm2_max);
  
  digitalWrite(13,HIGH); 

  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
  	#ifdef DEBUG_SERIAL
    Serial.println(F("SSD1306 allocation failed"));
    #endif
    for(;;); // Don't proceed, loop forever
  }
  display.setTextSize(1);      // Normal 1:1 pixel scale
  display.setTextColor(WHITE); // Draw white text
  display.setCursor(0, 0);
  display.cp437(true);

  thermistor = new AverageThermistor(
      new NTC_Thermistor(
      SENSOR_PIN,
      REFERENCE_RESISTANCE,
      NOMINAL_RESISTANCE,
      NOMINAL_TEMPERATURE,
      B_VALUE
    ),
    READINGS_NUMBER,
    DELAY_TIME
  );

#ifdef DEBUG_SERIAL
  Serial.begin(9600);
  Serial.println("Debug");
#endif
}
long new_pos=0;

void loop() {
  but_state[0]=!digitalRead(but1);  
  if(but_state[0]){
    delay(50);
    selector++;
    if(selector>2){
      selector=0;
    }
  }
  but_state[1]=!digitalRead(but2);
  if(but_state[1]){
    delay(50);
    but_state[1]=0;
#if USE_ATUNE
    tuning=true;
    /*lcd.clearDisplay();
    lcd.print("PID ");
    lcd.print(selector);
    lcd.print(" tuning...");*/
    if(selector==0){
      aTune.input=&temp1;
      aTune.output=&temp1_pwm;
    }else{
      aTune.input=&temp2;
      aTune.output=&temp2_pwm;
    }
#endif
  }
  
  but_state[2]=!digitalRead(but3);
  if(but_state[2]){ //t2 on/off
    delay(50);
    if(temp2_onoff){//turn t2 off
      temp2_onoff=0;
      temp2_pwm=0;
      analogWrite(pwm2_pin,temp2_pwm);
      analogWrite(pwm3_pin, fan_pwm);
    }else{//turn t2 on
      temp2_onoff=1;
      temp2PID.SetMode(AUTOMATIC);
      analogWrite(pwm3_pin, 255);
    }      
  }
  but_state[3]=!digitalRead(but4);
  if(but_state[3]){ //t1 on/off    
    delay(50);
    if(temp1_onoff){//turn t1 off
      temp1_onoff=0;
      temp1_pwm=0;
      analogWrite(pwm1_pin,temp1_pwm);
    }else{//turn t1 on
      temp1_onoff=1;
      temp1PID.SetMode(AUTOMATIC);
    }      
  }
   temp_amb = thermistor->readCelsius();
  
/*
  temp3_aux = analogRead(temp3_pin);  
  short t1,t2;
  temp3= 0;
  for(short o=0;o<18*2;o+=2){        
    if(temp3_aux<=temptable[o]){
      if(o>0){
        t2=temptable[o+1];
        t1=temptable[o-1];
        temp3= (t1+t2)/2;
      }else{
        temp3= 200;
      }
      break;
    }
  }  */
  
  //temp1 = analogRead(temp1_pin)*typekReadingScaler;
  temp1 = analogRead(temp1_pin);
  delay(10);
  temp2 = analogRead(temp2_pin)*typekReadingScaler;
  delay(10);
  #if USE_ATUNE
  if(tuning && !tuning_ended)
  {
    byte val = (aTune.Runtime());
    if (val!=0)
    {
      tuning_ended=true;
    }
    if(tuning_ended)
    {       
      /*lcd.clearDisplay();
      lcd.setCursor(0, 0);        
      lcd.print("kp:");
      lcd.print(aTune.GetKp());
      lcd.setCursor(0, 10);  
      lcd.print("ki:");
      lcd.print(aTune.GetKi());
      lcd.setCursor(0, 20);  
      lcd.print("kd:");
      lcd.print(aTune.GetKd());    */
    }
  }else
#endif
  {
    if(temp1_onoff){
      temp1PID.Compute();
      analogWrite(pwm1_pin,temp1_pwm);
    }
    if(temp2_onoff){    
      temp2PID.Compute();
      analogWrite(pwm2_pin,temp2_pwm);    
    }	
    new_pos = knob.read();
    if (  new_pos != knob_pos) {
      int inc=new_pos-knob_pos;
      if (inc>0) {
        inc=1;
      }else{
        inc=-1;
      }
      switch(selector){
        case 0:
          temp1_setpoint+=inc;        
        break;
        case 1:
          temp2_setpoint+=inc;      
        break;
        case 2:
          if((fan_pwm+inc)>=0 && (fan_pwm+inc)<=255){
            fan_pwm+=inc;
            analogWrite(pwm3_pin, fan_pwm);
          }
        break;
      } 
        
      knob_pos=new_pos;
    }   

    
    display.clearDisplay();
    display.setCursor(0, 0);  

    display.setTextSize(1);
    display.print(F("T1 "));    
    display.print(temp1);
    display.print(F(" "));    
    display.print(temp1*4.9);
    display.println(F("mV"));    
    display.print(F("T3 "));
    display.println(temp_amb);

    display.display();
    /*lcd.clearDisplay();
    lcd.setCursor(0, 0);  
    if(selector==0){
      lcd.print("(*)");
    }else{
      lcd.print("( )");
    }
    lcd.print("T1 ");
    lcd.print(temp1,0);  
    lcd.print("/");
    lcd.print(temp1_setpoint,0);
    lcd.print((char)247);
    lcd.print("C "); 
    lcd.setCursor(0, 10);
    if(selector==1){
      lcd.print("(*)");
    }else{
      lcd.print("( )");
    }
    lcd.print("T2 ");
    lcd.print(temp2,0);  
    lcd.print("/");
    lcd.print(temp2_setpoint,0);
    lcd.print((char)247);
    lcd.print("C ");
    lcd.setCursor(0, 20);
    if(selector==2){
      lcd.print("(*)");
    }else{
      lcd.print("( )");
    }
    lcd.print("fan_pwm:");
    lcd.print(fan_pwm,0);
    //lcd.print(knob_pos);
    //lcd.print(temp3_aux);    
    //lcd.print(" a: ");
    //lcd.print(temp3);
    //lcd.print((char)247);
    //lcd.print("C ");     
    lcd.setCursor(0, 40);
    lcd.print("pwm1:");
    lcd.print(temp1_pwm,0);
    if(temp1_onoff){
      lcd.print(" on");  
    }else{
      lcd.print(" off");
    }
    lcd.setCursor(0, 50);
    lcd.print("pwm2:");
    lcd.print(temp2_pwm,0);
    if(temp2_onoff){
      lcd.print(" on");  
    }else{
      lcd.print(" off");
    }
    lcd.display();
    */
  }
}
