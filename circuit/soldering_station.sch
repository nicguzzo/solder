EESchema Schematic File Version 4
LIBS:soldering_station-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:LM324 U3
U 1 1 5DA5715A
P 7100 3350
F 0 "U3" H 7050 3300 50  0000 C CNN
F 1 "LM324" H 7100 3626 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_DIP-14_W7.62mm_Socket_LongPads" H 7050 3450 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 7150 3550 50  0001 C CNN
	1    7100 3350
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM324 U3
U 2 1 5DA58026
P 7100 4600
F 0 "U3" H 7100 4600 50  0000 C CNN
F 1 "LM324" H 7050 5000 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_DIP-14_W7.62mm_Socket_LongPads" H 7050 4700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 7150 4800 50  0001 C CNN
	2    7100 4600
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:LM324 U3
U 3 1 5DA58F30
P 8250 3900
F 0 "U3" H 8250 4267 50  0000 C CNN
F 1 "LM324" H 8250 4176 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_DIP-14_W7.62mm_Socket_LongPads" H 8200 4000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 8300 4100 50  0001 C CNN
	3    8250 3900
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM324 U3
U 5 1 5DA5A84D
P 5800 3950
F 0 "U3" H 5758 3996 50  0000 L CNN
F 1 "LM324" H 5758 3905 50  0000 L CNN
F 2 "tht_as_smd:tht_smd_DIP-14_W7.62mm_Socket_LongPads" H 5750 4050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 5850 4150 50  0001 C CNN
	5    5800 3950
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR012
U 1 1 5DA6054B
P 5700 3450
F 0 "#PWR012" H 5700 3300 50  0001 C CNN
F 1 "+5V" H 5715 3623 50  0000 C CNN
F 2 "" H 5700 3450 50  0001 C CNN
F 3 "" H 5700 3450 50  0001 C CNN
	1    5700 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5DA60A7B
P 5700 4400
F 0 "#PWR013" H 5700 4150 50  0001 C CNN
F 1 "GND" H 5705 4227 50  0000 C CNN
F 2 "" H 5700 4400 50  0001 C CNN
F 3 "" H 5700 4400 50  0001 C CNN
	1    5700 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 4250 5700 4350
Wire Wire Line
	5700 3450 5700 3550
$Comp
L Device:R_US R6
U 1 1 5DA61496
P 7250 3950
F 0 "R6" H 7318 3996 50  0000 L CNN
F 1 "1k" H 7318 3905 50  0000 L CNN
F 2 "tht_as_smd:tht_smd_res" V 7290 3940 50  0001 C CNN
F 3 "~" H 7250 3950 50  0001 C CNN
	1    7250 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R4
U 1 1 5DA62748
P 7050 3700
F 0 "R4" V 6845 3700 50  0000 C CNN
F 1 "1k" V 6936 3700 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_small_res" V 7090 3690 50  0001 C CNN
F 3 "~" H 7050 3700 50  0001 C CNN
	1    7050 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R5
U 1 1 5DA62E54
P 7050 4200
F 0 "R5" V 6845 4200 50  0000 C CNN
F 1 "1k" V 6936 4200 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_small_res" V 7090 4190 50  0001 C CNN
F 3 "~" H 7050 4200 50  0001 C CNN
	1    7050 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	6800 3450 6750 3450
Wire Wire Line
	6750 3450 6750 3700
Wire Wire Line
	6750 3700 6900 3700
Wire Wire Line
	7200 3700 7250 3700
Wire Wire Line
	7450 3700 7450 3350
Wire Wire Line
	7450 3350 7400 3350
Wire Wire Line
	7250 3800 7250 3700
Connection ~ 7250 3700
Wire Wire Line
	7250 3700 7450 3700
Wire Wire Line
	6900 4200 6700 4200
Wire Wire Line
	6700 4200 6700 4500
Wire Wire Line
	6700 4500 6800 4500
Wire Wire Line
	7200 4200 7250 4200
Wire Wire Line
	7250 4200 7250 4100
Wire Wire Line
	7250 4200 7450 4200
Wire Wire Line
	7450 4200 7450 4600
Wire Wire Line
	7450 4600 7400 4600
Connection ~ 7250 4200
$Comp
L Connector:Screw_Terminal_01x02 J5
U 1 1 5DA67471
P 6150 3950
F 0 "J5" H 6068 3625 50  0000 C CNN
F 1 "TC" H 6068 3716 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 6150 3950 50  0001 C CNN
F 3 "~" H 6150 3950 50  0001 C CNN
	1    6150 3950
	-1   0    0    1   
$EndComp
Wire Wire Line
	6350 3850 6350 3250
Wire Wire Line
	6350 3250 6800 3250
Wire Wire Line
	6350 3950 6350 4700
Wire Wire Line
	6350 4700 6800 4700
$Comp
L Device:R_US R8
U 1 1 5DA693F5
P 7700 3800
F 0 "R8" V 7495 3800 50  0000 C CNN
F 1 "1k" V 7586 3800 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_small_res" V 7740 3790 50  0001 C CNN
F 3 "~" H 7700 3800 50  0001 C CNN
	1    7700 3800
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R9
U 1 1 5DA69982
P 7700 4000
F 0 "R9" V 7950 4000 50  0000 C CNN
F 1 "1k" V 7850 4000 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_small_res" V 7740 3990 50  0001 C CNN
F 3 "~" H 7700 4000 50  0001 C CNN
	1    7700 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	7950 3800 7900 3800
Wire Wire Line
	7950 4000 7900 4000
Wire Wire Line
	7550 3800 7550 3700
Wire Wire Line
	7550 3700 7450 3700
Connection ~ 7450 3700
Wire Wire Line
	7450 4200 7550 4200
Wire Wire Line
	7550 4200 7550 4000
Connection ~ 7450 4200
$Comp
L Device:R_US R13
U 1 1 5DA6BF54
P 8800 3900
F 0 "R13" V 9050 3900 50  0000 C CNN
F 1 "100k" V 8950 3900 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_res" V 8840 3890 50  0001 C CNN
F 3 "~" H 8800 3900 50  0001 C CNN
	1    8800 3900
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R11
U 1 1 5DA6CCF8
P 8350 4450
F 0 "R11" V 8250 4350 50  0000 C CNN
F 1 "100k" V 8500 4450 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_res" V 8390 4440 50  0001 C CNN
F 3 "~" H 8350 4450 50  0001 C CNN
	1    8350 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	8200 4450 7900 4450
Wire Wire Line
	7900 4450 7900 4000
Connection ~ 7900 4000
Wire Wire Line
	7900 4000 7850 4000
Wire Wire Line
	8500 4450 8600 4450
Wire Wire Line
	8600 4450 8600 3900
Wire Wire Line
	8600 3900 8550 3900
Wire Wire Line
	8650 3900 8600 3900
Connection ~ 8600 3900
Wire Wire Line
	7900 3800 7900 3300
Connection ~ 7900 3800
Wire Wire Line
	7900 3800 7850 3800
$Comp
L power:GND #PWR016
U 1 1 5DA70D57
P 8500 3400
F 0 "#PWR016" H 8500 3150 50  0001 C CNN
F 1 "GND" H 8505 3227 50  0000 C CNN
F 2 "" H 8500 3400 50  0001 C CNN
F 3 "" H 8500 3400 50  0001 C CNN
	1    8500 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 3300 8500 3400
Wire Wire Line
	8950 3900 9050 3900
Text GLabel 9050 3900 2    50   Input ~ 0
tcout
$Comp
L Diode:1N4148 D2
U 1 1 5DA7A1CA
P 6600 2050
F 0 "D2" H 6600 1834 50  0000 C CNN
F 1 "1N4148" H 6600 1925 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_D_DO-35_SOD27_P7.62mm_Horizontal" H 6600 1875 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6600 2050 50  0001 C CNN
	1    6600 2050
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C8
U 1 1 5DA7A7F2
P 5300 3950
F 0 "C8" H 5392 3996 50  0000 L CNN
F 1 "100nf" H 5150 3700 50  0000 L CNN
F 2 "Capacitor_SMD:C_1806_4516Metric_Pad1.57x1.80mm_HandSolder" H 5300 3950 50  0001 C CNN
F 3 "~" H 5300 3950 50  0001 C CNN
	1    5300 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C10
U 1 1 5DA7B098
P 9050 4150
F 0 "C10" H 8900 3950 50  0000 L CNN
F 1 "10nf" H 8900 3850 50  0000 L CNN
F 2 "Capacitor_SMD:C_1806_4516Metric_Pad1.57x1.80mm_HandSolder" H 9050 4150 50  0001 C CNN
F 3 "~" H 9050 4150 50  0001 C CNN
	1    9050 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3550 5700 3550
Connection ~ 5700 3550
Wire Wire Line
	5700 3550 5700 3650
Wire Wire Line
	5300 4350 5300 4050
Wire Wire Line
	5300 4350 5700 4350
Connection ~ 5700 4350
Wire Wire Line
	5700 4350 5700 4400
Wire Wire Line
	9050 4050 9050 3900
$Comp
L power:GND #PWR018
U 1 1 5DA80B12
P 9050 4550
F 0 "#PWR018" H 9050 4300 50  0001 C CNN
F 1 "GND" H 9055 4377 50  0000 C CNN
F 2 "" H 9050 4550 50  0001 C CNN
F 3 "" H 9050 4550 50  0001 C CNN
	1    9050 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 4250 9050 4550
$Comp
L Device:CP C9
U 1 1 5DA83DAF
P 6200 2250
F 0 "C9" H 6318 2296 50  0000 L CNN
F 1 "10uf" H 6318 2205 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x3" H 6238 2100 50  0001 C CNN
F 3 "~" H 6200 2250 50  0001 C CNN
	1    6200 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R3
U 1 1 5DA85103
P 6350 1750
F 0 "R3" H 6418 1796 50  0000 L CNN
F 1 "15k" H 6418 1705 50  0000 L CNN
F 2 "tht_as_smd:tht_smd_res" V 6390 1740 50  0001 C CNN
F 3 "~" H 6350 1750 50  0001 C CNN
	1    6350 1750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5DA873AF
P 6550 2500
F 0 "#PWR014" H 6550 2250 50  0001 C CNN
F 1 "GND" H 6555 2327 50  0000 C CNN
F 2 "" H 6550 2500 50  0001 C CNN
F 3 "" H 6550 2500 50  0001 C CNN
	1    6550 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2050 6850 2050
Wire Wire Line
	6350 1600 6350 1400
Wire Wire Line
	6450 2050 6350 2050
Wire Wire Line
	6350 2050 6350 1900
Wire Wire Line
	6200 2400 6200 2450
Wire Wire Line
	6200 2450 6550 2450
Wire Wire Line
	6550 2450 6550 2500
Wire Wire Line
	6850 2450 6550 2450
Wire Wire Line
	6850 2050 6850 2450
Connection ~ 6550 2450
Wire Wire Line
	6350 2050 6200 2050
Wire Wire Line
	6200 2050 6200 2100
Connection ~ 6350 2050
Text GLabel 6350 1400 1    50   Input ~ 0
aref
Text GLabel 6000 2050 0    50   Input ~ 0
cjc
Connection ~ 6200 2050
Wire Wire Line
	6000 2050 6200 2050
$Comp
L Transistor_FET:IRF3205 Q1
U 1 1 5DA9FC52
P 9400 2000
F 0 "Q1" H 9606 2046 50  0000 L CNN
F 1 "IRF3205" H 9606 1955 50  0000 L CNN
F 2 "tht_as_smd:tht_smd_TO-220-3_Vertical" H 9650 1925 50  0001 L CIN
F 3 "http://www.irf.com/product-info/datasheets/data/irf3205.pdf" H 9400 2000 50  0001 L CNN
	1    9400 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5DAA12D9
P 9500 2500
F 0 "#PWR020" H 9500 2250 50  0001 C CNN
F 1 "GND" H 9505 2327 50  0000 C CNN
F 2 "" H 9500 2500 50  0001 C CNN
F 3 "" H 9500 2500 50  0001 C CNN
	1    9500 2500
	1    0    0    -1  
$EndComp
$Comp
L Isolator:PC817 U4
U 1 1 5DAA1A16
P 8250 1900
F 0 "U4" H 8250 2225 50  0000 C CNN
F 1 "PC817" H 8250 2134 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_DIP-4_W7.62mm" H 8050 1700 50  0001 L CIN
F 3 "http://www.soselectronic.cz/a_info/resource/d/pc817.pdf" H 8250 1900 50  0001 L CNN
	1    8250 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R14
U 1 1 5DAA293F
P 8950 2350
F 0 "R14" V 8850 2250 50  0000 C CNN
F 1 "100k" V 9100 2350 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_res" V 8990 2340 50  0001 C CNN
F 3 "~" H 8950 2350 50  0001 C CNN
	1    8950 2350
	-1   0    0    1   
$EndComp
$Comp
L power:+12V #PWR019
U 1 1 5DAA358E
P 9500 1300
F 0 "#PWR019" H 9500 1150 50  0001 C CNN
F 1 "+12V" H 9515 1473 50  0000 C CNN
F 2 "" H 9500 1300 50  0001 C CNN
F 3 "" H 9500 1300 50  0001 C CNN
	1    9500 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 2200 9500 2500
$Comp
L power:GND #PWR017
U 1 1 5DAA9072
P 8950 2550
F 0 "#PWR017" H 8950 2300 50  0001 C CNN
F 1 "GND" H 8955 2377 50  0000 C CNN
F 2 "" H 8950 2550 50  0001 C CNN
F 3 "" H 8950 2550 50  0001 C CNN
	1    8950 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 2500 8950 2550
Wire Wire Line
	8950 2200 8950 2000
Wire Wire Line
	8950 2000 9200 2000
$Comp
L Device:R_US R12
U 1 1 5DAAD019
P 8700 1550
F 0 "R12" V 8600 1450 50  0000 C CNN
F 1 "10k" V 8850 1550 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_res" V 8740 1540 50  0001 C CNN
F 3 "~" H 8700 1550 50  0001 C CNN
	1    8700 1550
	-1   0    0    1   
$EndComp
Wire Wire Line
	9500 1350 8700 1350
Wire Wire Line
	8700 1350 8700 1400
Wire Wire Line
	9500 1300 9500 1350
Wire Wire Line
	8700 1700 8700 1800
Wire Wire Line
	8550 1800 8700 1800
Connection ~ 8950 2000
$Comp
L Device:R_US R7
U 1 1 5DAB7E67
P 7600 1800
F 0 "R7" V 7500 1700 50  0000 C CNN
F 1 "1k" V 7750 1800 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_res" V 7640 1790 50  0001 C CNN
F 3 "~" H 7600 1800 50  0001 C CNN
	1    7600 1800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7950 1800 7750 1800
$Comp
L power:GND #PWR015
U 1 1 5DAC17C6
P 7850 2150
F 0 "#PWR015" H 7850 1900 50  0001 C CNN
F 1 "GND" H 7855 1977 50  0000 C CNN
F 2 "" H 7850 2150 50  0001 C CNN
F 3 "" H 7850 2150 50  0001 C CNN
	1    7850 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 2000 7850 2000
Wire Wire Line
	7850 2000 7850 2150
Text GLabel 7300 1800 0    50   Input ~ 0
pwm
Wire Wire Line
	7300 1800 7450 1800
$Comp
L MCU_Microchip_ATmega:ATmega328-PU U1
U 1 1 5DACC33E
P 2450 3150
F 0 "U1" H 1806 3196 50  0000 R CNN
F 1 "ATmega328-PU" H 1806 3105 50  0000 R CNN
F 2 "tht_as_smd:thtsmd_DIP-28_W7.62mm" H 2450 3150 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 2450 3150 50  0001 C CNN
	1    2450 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal Y1
U 1 1 5DAE475E
P 3500 2600
F 0 "Y1" V 3454 2731 50  0000 L CNN
F 1 "20Mhz" V 3545 2731 50  0000 L CNN
F 2 "tht_as_smd:tht_smd_Crystal_HC18-U_Vertical" H 3500 2600 50  0001 C CNN
F 3 "~" H 3500 2600 50  0001 C CNN
	1    3500 2600
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C5
U 1 1 5DAE595C
P 4050 2650
F 0 "C5" H 4142 2696 50  0000 L CNN
F 1 "22pf" H 4150 2800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4050 2650 50  0001 C CNN
F 3 "~" H 4050 2650 50  0001 C CNN
	1    4050 2650
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C4
U 1 1 5DAE7ACC
P 4050 2500
F 0 "C4" H 3850 2550 50  0000 L CNN
F 1 "22pf" H 3800 2650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4050 2500 50  0001 C CNN
F 3 "~" H 4050 2500 50  0001 C CNN
	1    4050 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	3350 2450 3350 2550
Wire Wire Line
	3050 2550 3350 2550
Wire Wire Line
	3350 2450 3500 2450
Wire Wire Line
	3350 2650 3350 2750
Wire Wire Line
	3350 2750 3500 2750
Wire Wire Line
	3500 2750 3950 2750
Wire Wire Line
	3950 2750 3950 2650
Connection ~ 3500 2750
Wire Wire Line
	3500 2450 3950 2450
Wire Wire Line
	3950 2450 3950 2500
Connection ~ 3500 2450
$Comp
L power:GND #PWR08
U 1 1 5DAFE74F
P 4400 2750
F 0 "#PWR08" H 4400 2500 50  0001 C CNN
F 1 "GND" H 4405 2577 50  0000 C CNN
F 2 "" H 4400 2750 50  0001 C CNN
F 3 "" H 4400 2750 50  0001 C CNN
	1    4400 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 2500 4400 2500
Wire Wire Line
	4400 2500 4400 2650
Wire Wire Line
	4150 2650 4400 2650
Connection ~ 4400 2650
Wire Wire Line
	4400 2650 4400 2750
Wire Wire Line
	3050 2650 3350 2650
Text GLabel 1750 1950 0    50   Input ~ 0
aref
$Comp
L power:+5V #PWR02
U 1 1 5DB13E96
P 2450 1450
F 0 "#PWR02" H 2450 1300 50  0001 C CNN
F 1 "+5V" H 2465 1623 50  0000 C CNN
F 2 "" H 2450 1450 50  0001 C CNN
F 3 "" H 2450 1450 50  0001 C CNN
	1    2450 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 1650 2450 1550
Wire Wire Line
	2550 1650 2550 1550
Wire Wire Line
	2550 1550 2450 1550
Connection ~ 2450 1550
Wire Wire Line
	2450 1550 2450 1450
$Comp
L power:GND #PWR04
U 1 1 5DB1B753
P 3100 1600
F 0 "#PWR04" H 3100 1350 50  0001 C CNN
F 1 "GND" H 3105 1427 50  0000 C CNN
F 2 "" H 3100 1600 50  0001 C CNN
F 3 "" H 3100 1600 50  0001 C CNN
	1    3100 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5DB1AF0D
P 2850 1550
F 0 "C2" H 2942 1596 50  0000 L CNN
F 1 "100nf" V 2650 1500 50  0000 L CNN
F 2 "Capacitor_SMD:C_1806_4516Metric_Pad1.57x1.80mm_HandSolder" H 2850 1550 50  0001 C CNN
F 3 "~" H 2850 1550 50  0001 C CNN
	1    2850 1550
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 1600 3100 1550
Wire Wire Line
	3100 1550 2950 1550
Wire Wire Line
	2750 1550 2550 1550
Connection ~ 2550 1550
Wire Wire Line
	5300 3550 5300 3850
$Comp
L Device:R_US R1
U 1 1 5DB3A193
P 3700 3300
F 0 "R1" H 3768 3346 50  0000 L CNN
F 1 "10k" H 3768 3255 50  0000 L CNN
F 2 "tht_as_smd:tht_smd_res" V 3740 3290 50  0001 C CNN
F 3 "~" H 3700 3300 50  0001 C CNN
	1    3700 3300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR06
U 1 1 5DB3B510
P 3700 3050
F 0 "#PWR06" H 3700 2900 50  0001 C CNN
F 1 "+5V" H 3715 3223 50  0000 C CNN
F 2 "" H 3700 3050 50  0001 C CNN
F 3 "" H 3700 3050 50  0001 C CNN
	1    3700 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 3050 3700 3150
Wire Wire Line
	3700 3450 3050 3450
$Comp
L Device:C_Small C1
U 1 1 5DB42ECB
P 1800 2150
F 0 "C1" H 1892 2196 50  0000 L CNN
F 1 "100nf" H 1400 2150 50  0000 L CNN
F 2 "Capacitor_SMD:C_1806_4516Metric_Pad1.57x1.80mm_HandSolder" H 1800 2150 50  0001 C CNN
F 3 "~" H 1800 2150 50  0001 C CNN
	1    1800 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 2050 1800 1950
Wire Wire Line
	1750 1950 1800 1950
Connection ~ 1800 1950
Wire Wire Line
	1800 1950 1850 1950
$Comp
L power:GND #PWR01
U 1 1 5DB473EC
P 1800 2350
F 0 "#PWR01" H 1800 2100 50  0001 C CNN
F 1 "GND" H 1805 2177 50  0000 C CNN
F 2 "" H 1800 2350 50  0001 C CNN
F 3 "" H 1800 2350 50  0001 C CNN
	1    1800 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 2250 1800 2350
Text GLabel 3150 2950 2    50   Input ~ 0
cjc
Text GLabel 3200 3150 2    50   Input ~ 0
tcout
Wire Wire Line
	3150 2950 3050 2950
Text GLabel 3250 3350 2    50   Input ~ 0
SCL
Text GLabel 3250 3250 2    50   Input ~ 0
SDA
Wire Wire Line
	3050 3250 3250 3250
Wire Wire Line
	3250 3350 3050 3350
$Comp
L Regulator_Linear:L7805 U2
U 1 1 5DB66C04
P 4100 5850
F 0 "U2" H 4100 6092 50  0000 C CNN
F 1 "L7805" H 4100 6001 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_TO-220-3_Vertical" H 4125 5700 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 4100 5800 50  0001 C CNN
	1    4100 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5DB684D7
P 2450 4750
F 0 "#PWR03" H 2450 4500 50  0001 C CNN
F 1 "GND" H 2455 4577 50  0000 C CNN
F 2 "" H 2450 4750 50  0001 C CNN
F 3 "" H 2450 4750 50  0001 C CNN
	1    2450 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5DB6880B
P 4100 6600
F 0 "#PWR07" H 4100 6350 50  0001 C CNN
F 1 "GND" H 4105 6427 50  0000 C CNN
F 2 "" H 4100 6600 50  0001 C CNN
F 3 "" H 4100 6600 50  0001 C CNN
	1    4100 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 4750 2450 4650
Wire Wire Line
	4100 6600 4100 6450
$Comp
L Device:CP C7
U 1 1 5DB718C8
P 4800 6200
F 0 "C7" H 4918 6246 50  0000 L CNN
F 1 "47uf" H 4918 6155 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x3" H 4838 6050 50  0001 C CNN
F 3 "~" H 4800 6200 50  0001 C CNN
	1    4800 6200
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C3
U 1 1 5DB71E55
P 3550 6200
F 0 "C3" H 3668 6246 50  0000 L CNN
F 1 "47uf" H 3668 6155 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x3" H 3588 6050 50  0001 C CNN
F 3 "~" H 3550 6200 50  0001 C CNN
	1    3550 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 6050 3550 5850
Wire Wire Line
	3550 5850 3800 5850
Wire Wire Line
	4400 5850 4500 5850
Wire Wire Line
	4800 5850 4800 6050
Wire Wire Line
	4800 6350 4800 6450
Wire Wire Line
	4800 6450 4500 6450
Connection ~ 4100 6450
Wire Wire Line
	4100 6450 4100 6150
Wire Wire Line
	3550 6350 3550 6450
Wire Wire Line
	3550 6450 4100 6450
$Comp
L Device:C_Small C6
U 1 1 5DB8E8AA
P 4500 6150
F 0 "C6" H 4592 6196 50  0000 L CNN
F 1 "100nf" H 4250 6000 50  0000 L CNN
F 2 "Capacitor_SMD:C_1806_4516Metric_Pad1.57x1.80mm_HandSolder" H 4500 6150 50  0001 C CNN
F 3 "~" H 4500 6150 50  0001 C CNN
	1    4500 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 6050 4500 5850
Connection ~ 4500 5850
Wire Wire Line
	4500 5850 4800 5850
Wire Wire Line
	4500 6250 4500 6450
Connection ~ 4500 6450
Wire Wire Line
	4500 6450 4100 6450
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 5DB99B95
P 2750 6100
F 0 "J1" H 2668 6317 50  0000 C CNN
F 1 "PSU" H 2668 6226 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 2750 6100 50  0001 C CNN
F 3 "~" H 2750 6100 50  0001 C CNN
	1    2750 6100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2950 6100 3250 6100
Wire Wire Line
	3250 6100 3250 5850
Wire Wire Line
	3250 5850 3550 5850
Connection ~ 3550 5850
Wire Wire Line
	2950 6450 3550 6450
Connection ~ 3550 6450
$Comp
L power:+5V #PWR011
U 1 1 5DBAC92F
P 4800 5700
F 0 "#PWR011" H 4800 5550 50  0001 C CNN
F 1 "+5V" H 4815 5873 50  0000 C CNN
F 2 "" H 4800 5700 50  0001 C CNN
F 3 "" H 4800 5700 50  0001 C CNN
	1    4800 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 5700 4800 5850
Connection ~ 4800 5850
$Comp
L Connector:Screw_Terminal_01x02 J6
U 1 1 5DBC8442
P 9800 1550
F 0 "J6" H 9880 1542 50  0000 L CNN
F 1 "HEATER" H 9880 1451 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 9800 1550 50  0001 C CNN
F 3 "~" H 9800 1550 50  0001 C CNN
	1    9800 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 1550 9500 1550
Wire Wire Line
	9500 1550 9500 1350
Connection ~ 9500 1350
Wire Wire Line
	9600 1650 9500 1650
Wire Wire Line
	9500 1650 9500 1800
$Comp
L power:+12V #PWR05
U 1 1 5DBD5DE5
P 3250 5650
F 0 "#PWR05" H 3250 5500 50  0001 C CNN
F 1 "+12V" H 3265 5823 50  0000 C CNN
F 2 "" H 3250 5650 50  0001 C CNN
F 3 "" H 3250 5650 50  0001 C CNN
	1    3250 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 5650 3250 5850
Connection ~ 3250 5850
Wire Wire Line
	8550 2000 8950 2000
$Comp
L power:GND #PWR021
U 1 1 5DC0AF46
P 9700 3650
F 0 "#PWR021" H 9700 3400 50  0001 C CNN
F 1 "GND" H 9705 3477 50  0000 C CNN
F 2 "" H 9700 3650 50  0001 C CNN
F 3 "" H 9700 3650 50  0001 C CNN
	1    9700 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R2
U 1 1 5DC39202
P 5300 6000
F 0 "R2" H 5368 6046 50  0000 L CNN
F 1 "1k" H 5368 5955 50  0000 L CNN
F 2 "tht_as_smd:tht_smd_res" V 5340 5990 50  0001 C CNN
F 3 "~" H 5300 6000 50  0001 C CNN
	1    5300 6000
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5DC39C3F
P 5150 6300
F 0 "D1" V 5189 6183 50  0000 R CNN
F 1 "LED" V 5098 6183 50  0000 R CNN
F 2 "LED_SMD:LED_2816_7142Metric_Pad3.20x4.45mm_HandSolder" H 5150 6300 50  0001 C CNN
F 3 "~" H 5150 6300 50  0001 C CNN
	1    5150 6300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5150 6450 4800 6450
Connection ~ 4800 6450
Wire Wire Line
	5300 6150 5150 6150
Wire Wire Line
	4800 5850 5300 5850
$Comp
L power:+5V #PWR09
U 1 1 5DC78945
P 4450 3400
F 0 "#PWR09" H 4450 3250 50  0001 C CNN
F 1 "+5V" H 4500 3650 50  0000 C CNN
F 2 "" H 4450 3400 50  0001 C CNN
F 3 "" H 4450 3400 50  0001 C CNN
	1    4450 3400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5DC78D44
P 4450 3500
F 0 "#PWR010" H 4450 3250 50  0001 C CNN
F 1 "GND" H 4455 3327 50  0000 C CNN
F 2 "" H 4450 3500 50  0001 C CNN
F 3 "" H 4450 3500 50  0001 C CNN
	1    4450 3500
	0    1    1    0   
$EndComp
Text GLabel 4550 3300 0    50   Input ~ 0
SDA
Text GLabel 4550 3200 0    50   Input ~ 0
SCL
Wire Wire Line
	4600 3500 4450 3500
Wire Wire Line
	4600 3400 4450 3400
Wire Wire Line
	4600 3300 4550 3300
Wire Wire Line
	4600 3200 4550 3200
Wire Wire Line
	7900 3300 8000 3300
Wire Wire Line
	8300 3300 8500 3300
$Comp
L Device:R_US R10
U 1 1 5DA6C8CB
P 8150 3300
F 0 "R10" V 8050 3200 50  0000 C CNN
F 1 "100k" V 8300 3300 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_res" V 8190 3290 50  0001 C CNN
F 3 "~" H 8150 3300 50  0001 C CNN
	1    8150 3300
	0    1    1    0   
$EndComp
Text GLabel 3200 1950 2    50   Input ~ 0
pwm
Wire Wire Line
	3200 1950 3050 1950
$Comp
L Connector_Generic:Conn_01x05 J2
U 1 1 5DCBF516
P 3250 2250
F 0 "J2" H 3330 2292 50  0000 L CNN
F 1 "pb" H 3330 2201 50  0000 L CNN
F 2 "tht_as_smd:tht_smd_PinHeader_1x05_P2.54mm_Vertical" H 3250 2250 50  0001 C CNN
F 3 "~" H 3250 2250 50  0001 C CNN
	1    3250 2250
	1    0    0    -1  
$EndComp
Text GLabel 3150 3650 2    50   Input ~ 0
rx
Text GLabel 3150 3750 2    50   Input ~ 0
tx
Wire Wire Line
	3150 3650 3050 3650
Wire Wire Line
	3150 3750 3050 3750
Text GLabel 3950 4200 0    50   Input ~ 0
rx
Text GLabel 3950 4100 0    50   Input ~ 0
tx
$Comp
L power:GND #PWR023
U 1 1 5DCE150A
P 3900 4300
F 0 "#PWR023" H 3900 4050 50  0001 C CNN
F 1 "GND" H 3905 4127 50  0000 C CNN
F 2 "" H 3900 4300 50  0001 C CNN
F 3 "" H 3900 4300 50  0001 C CNN
	1    3900 4300
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 4300 4050 4300
Wire Wire Line
	4050 4200 3950 4200
Wire Wire Line
	4050 4100 3950 4100
$Comp
L Connector:Screw_Terminal_01x02 J8
U 1 1 5DD0B527
P 5850 6250
F 0 "J8" H 5768 5925 50  0000 C CNN
F 1 "5v PSU" H 5768 6016 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 5850 6250 50  0001 C CNN
F 3 "~" H 5850 6250 50  0001 C CNN
	1    5850 6250
	1    0    0    1   
$EndComp
Wire Wire Line
	5300 5850 5650 5850
Connection ~ 5300 5850
Wire Wire Line
	5650 6250 5650 6450
Wire Wire Line
	5650 6450 5150 6450
Connection ~ 5150 6450
$Comp
L Connector_Generic:Conn_01x03 J7
U 1 1 5DD1DF0C
P 4250 4200
F 0 "J7" H 4330 4242 50  0000 L CNN
F 1 "serial" H 4330 4151 50  0000 L CNN
F 2 "tht_as_smd:tht_smd_PinHeader_1x03_P2.54mm_Vertical" H 4250 4200 50  0001 C CNN
F 3 "~" H 4250 4200 50  0001 C CNN
	1    4250 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R15
U 1 1 5DDA3BA4
P 9700 3050
F 0 "R15" V 9600 2950 50  0000 C CNN
F 1 "10k" V 9850 3050 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_res" V 9740 3040 50  0001 C CNN
F 3 "~" H 9700 3050 50  0001 C CNN
	1    9700 3050
	-1   0    0    1   
$EndComp
$Comp
L Amplifier_Operational:LM324 U3
U 4 1 5DA59D7D
P 10300 3350
F 0 "U3" H 10300 3717 50  0000 C CNN
F 1 "LM324" H 10300 3626 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_DIP-14_W7.62mm_Socket_LongPads" H 10250 3450 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 10350 3550 50  0001 C CNN
	4    10300 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R16
U 1 1 5DDAF14A
P 9700 3450
F 0 "R16" V 9600 3350 50  0000 C CNN
F 1 "10k" V 9850 3450 50  0000 C CNN
F 2 "tht_as_smd:tht_smd_res" V 9740 3440 50  0001 C CNN
F 3 "~" H 9700 3450 50  0001 C CNN
	1    9700 3450
	-1   0    0    1   
$EndComp
Wire Wire Line
	9700 3200 9700 3250
Wire Wire Line
	10000 3250 9700 3250
Connection ~ 9700 3250
Wire Wire Line
	9700 3250 9700 3300
Wire Wire Line
	10000 3450 9950 3450
Wire Wire Line
	9950 3450 9950 3600
Wire Wire Line
	9950 3600 10650 3600
Wire Wire Line
	10650 3600 10650 3350
Wire Wire Line
	10650 3350 10600 3350
Wire Wire Line
	9700 3600 9700 3650
$Comp
L power:+5V #PWR022
U 1 1 5DDCFB01
P 9700 2850
F 0 "#PWR022" H 9700 2700 50  0001 C CNN
F 1 "+5V" H 9715 3023 50  0000 C CNN
F 2 "" H 9700 2850 50  0001 C CNN
F 3 "" H 9700 2850 50  0001 C CNN
	1    9700 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 2850 9700 2900
Wire Wire Line
	5650 5850 5650 6150
Wire Wire Line
	2950 6200 2950 6450
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 5DE7F7FC
P 3400 3950
F 0 "J3" H 3480 3992 50  0000 L CNN
F 1 "pd1" H 3480 3901 50  0000 L CNN
F 2 "tht_as_smd:tht_smd_PinHeader_1x03_P2.54mm_Vertical" H 3400 3950 50  0001 C CNN
F 3 "~" H 3400 3950 50  0001 C CNN
	1    3400 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3850 3200 3850
Wire Wire Line
	3050 3950 3200 3950
Wire Wire Line
	3200 4050 3050 4050
$Comp
L Connector_Generic:Conn_01x03 J9
U 1 1 5DE9A8FF
P 3400 4250
F 0 "J9" H 3480 4292 50  0000 L CNN
F 1 "pd2" H 3480 4201 50  0000 L CNN
F 2 "tht_as_smd:tht_smd_PinHeader_1x03_P2.54mm_Vertical" H 3400 4250 50  0001 C CNN
F 3 "~" H 3400 4250 50  0001 C CNN
	1    3400 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 4150 3200 4150
Wire Wire Line
	3050 4250 3200 4250
Wire Wire Line
	3050 4350 3200 4350
$Comp
L Connector_Generic:Conn_01x04 J4
U 1 1 5DED3A2B
P 4800 3300
F 0 "J4" H 4880 3292 50  0000 L CNN
F 1 "lcd" H 4880 3201 50  0000 L CNN
F 2 "tht_as_smd:tht_smd_PinHeader_1x04_P2.54mm_Vertical" H 4800 3300 50  0001 C CNN
F 3 "~" H 4800 3300 50  0001 C CNN
	1    4800 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3150 3200 3150
$EndSCHEMATC
